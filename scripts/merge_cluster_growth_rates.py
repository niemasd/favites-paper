#!/usr/bin/env python3
from sys import argv
sizes = {}
f_simples = []
for f in argv[1:]:
    f_simple = f.split('/')[-1]
    f_simples.append(f_simple)
    for l in open(f):
        if l.startswith('ClusterNumber'):
            continue
        c,r = l.split(','); c = int(c); r = float(r)
        if c not in sizes:
            sizes[c] = {}
        sizes[c][f_simple] = r
print("ClusterNumber,%s" % ','.join(f_simples))
for c in sorted(sizes.keys()):
    print(c, end='')
    for f in f_simples:
        if f in sizes[c]:
            print(",%f"%sizes[c][f], end='')
        else:
            print(",-", end='')
    print()
