#!/usr/bin/env python3
'''
Compute the growth rate of each cluster given clusterings at times t-1 and t.
'''
from gzip import open as gopen
import argparse
parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-c1', '--clustering1', required=True, type=str, help="Clustering 1 (t-1) (Cluster Picker format)")
parser.add_argument('-c2', '--clustering2', required=True, type=str, help="Clustering 2 (t) (Cluster Picker format)")
parser.add_argument('-o', '--output', required=False, default='stdout', help="Output File")
args = parser.parse_args()
if args.output == 'stdout':
    from sys import stdout; args.output = stdout
else:
    args.output = open(args.output,'w')
f = {}
if args.clustering1.lower().endswith('.gz'):
    f['c1'] = gopen(args.clustering1)
else:
    f['c1'] = open(args.clustering1)
if args.clustering2.lower().endswith('.gz'):
    f['c2'] = gopen(args.clustering2)
else:
    f['c2'] = open(args.clustering2)

# get cluster sizes
sizes = {'c1':{},'c2':{}}
for c in f:
    for l in f[c]:
        num = l.strip().split()[1]
        if isinstance(num,bytes):
            num = num.decode()
        if num[0] == '-' or num == 'ClusterNumber':
            continue
        num = int(num)
        if num not in sizes[c]:
            sizes[c][num] = 0
        sizes[c][num] += 1

# output cluster growth rates
args.output.write("ClusterNumber,GrowthRate\n")
for c in sorted(sizes['c1'].keys()):
    if c not in sizes['c2']:
        continue
    n1 = sizes['c1'][c]; n2 = sizes['c2'][c]
    growth = (n2-n1)/(n2**0.5) # denominator sqrt(n2)
    #growth = (n2-n1)/n2 # denominator n2
    args.output.write("%s,%f\n"%(c,growth))
