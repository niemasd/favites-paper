#!/usr/bin/env python3
from sys import argv
TR = {'1':'S', '2':'I1', '3':'I2', '6':'A1', '7':'A2'}
num = {k:0 for k in TR.values()}
num['I1'] = 15000 # seeds
out = {k:[0] for k in TR.values()}
out['I1'] = [15000] # seeds
T = 0.1
for line in open(argv[1]):
    t,r,n,b,a = line.split(' ')[:5]
    if b == '0':
        out['S'][0] += 1
    if b in TR:
        num[TR[b]] -= 1
    if a in TR:
        num[TR[a]] += 1
    if float(t) > T:
        T += 0.1
        for k in num:
            out[k].append(num[k])
for k in out:
    out[k].append(num[k])
print(str(out))
