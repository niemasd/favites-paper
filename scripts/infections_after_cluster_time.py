#!/usr/bin/env python3
'''
Compute the average number of infections after the clustering
'''
from gzip import open as gopen
from random import shuffle
import argparse
parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-c', '--clustering', required=True, type=str, help="Clustering (Cluster Picker format)")
parser.add_argument('-tn', '--transmissions', required=True, type=str, help="Transmission Network (FAVITES format)")
parser.add_argument('-t', '--time', required=True, type=float, help="Time at which Clustering was obtained")
parser.add_argument('-p', '--proportion', required=False, type=float, default=None, help="Proportion of \"Top\" Individuals")
parser.add_argument('-n', '--number', required=False, type=int, default=None, help="Number of \"Top\" Individuals")
parser.add_argument('-g', '--growthrates', required=False, type=str, default=None, help="Growth Rates (if not specified, sort by size)")
args = parser.parse_args()
if args.clustering.lower().endswith('.gz'):
    args.clustering = gopen(args.clustering)
else:
    args.clustering = open(args.clustering)
if args.transmissions.lower().endswith('.gz'):
    args.transmissions = gopen(args.transmissions)
else:
    args.transmissions = open(args.transmissions)
if args.growthrates is not None:
    if args.growthrates.lower().endswith('.gz'):
        args.growthrates = gopen(args.growthrates)
    else:
        args.growthrates = open(args.growthrates)
if args.proportion is None and args.number is None:
    assert False, "Must specify either --proportion or --number"
elif args.proportion is not None and args.number is not None:
    assert False, "Cannot specify both --proportion and --number: only specify one"

# read transmissions
trans = []; num_infected = 0
infected_individuals = set()
for l in args.transmissions:
    if isinstance(l,bytes):
        u,v,t = l.decode().strip().split()
    else:
        u,v,t = l.strip().split()
    t = float(t)
    trans.append((u,v,t))
    if t < args.time:
        num_infected += 1
    infected_individuals.add(v)

# read clusters
clusters = {}
for l in args.clustering:
    if isinstance(l,bytes):
        u,num = l.decode().strip().split()
    else:
        u,num = l.strip().split()
    if num[0] == '-' or num == 'ClusterNumber':
        continue
    u = u.split('|')[1]
    if num not in clusters:
        clusters[num] = set()
    clusters[num].add(u)
    infected_individuals.remove(u)
if args.growthrates is None:
    clusters = [list(cluster) for cluster in clusters.values()]
    for i in range(len(clusters)):
        shuffle(clusters[i])
    while len(infected_individuals) != 0:
        clusters.append([infected_individuals.pop()])
    clusters.sort(key=lambda e: len(e))
else:
    growths = {}
    for l in args.growthrates:
        if isinstance(l,bytes):
            num,gr = l.decode().strip().split(',')
        else:
            num,gr = l.strip().split(',')
        if num == 'ClusterNumber':
            continue
        growths[num] = float(gr)
    for num in clusters:
        if num not in growths:
            growths[num] = float('-inf')
    clusters = [(list(clusters[c]),growths[c]) for c in clusters]
    for i in range(len(clusters)):
        shuffle(clusters[i][0])
    while len(infected_individuals) != 0:
        clusters.append(([infected_individuals.pop()],float('-inf')))
    clusters.sort(key=lambda e: e[1])
    clusters = [e[0] for e in clusters]

# generate "top individuals"
top_individuals = set()
if args.number is not None:
    num_top_individuals = args.number
else:
    num_top_individuals = int(args.proportion*num_infected)
assert num_top_individuals > 0, "Must specify a positive number of top individuals"
curr = clusters.pop()
while len(top_individuals) < num_top_individuals:
    if len(curr) == 0:
        curr = clusters.pop()
    top_individuals.add(curr.pop())

# compute average number of infections from top individuals and from everyone after time t
tot_all = 0.
tot_top = 0.
for u,v,t in trans:
    if t < args.time:
        continue
    tot_all += 1
    if u in top_individuals:
        tot_top += 1
print("All: %f"%(tot_all/num_infected))
print("Top: %f"%(tot_top/len(top_individuals)))
