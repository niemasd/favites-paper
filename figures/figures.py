#! /usr/bin/env python3
from gzip import open as gopen
from pandas import DataFrame
from pickle import load
from sys import argv

# helper average function
def avg(x):
    return float(sum(x))/float(len(x))

# make x tick numbers integers instead of floats
def x_tick_int(div=10.):
    plt.gca().xaxis.set_major_formatter(FuncFormatter(lambda x, _: int(x/div)))

# read number list ([1,2,3,4,...]) from stream byte-by-byte
def read_numlist(f):
    out = []
    tmp = ''
    while True:
        c = f.read(1)
        if not c:
            break
        c = c.decode()
        if c in {'[',']'}:
            continue
        if c == ',':
            out.append(float(tmp)); tmp = ''
        else:
            tmp += c
    return out

# set up seaborn
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
from matplotlib.ticker import FormatStrFormatter,FuncFormatter
import numpy as np
import seaborn as sns
#sns.set(font_scale=1.25)
sns.set_context("paper", rc={"font.size":12,"axes.titlesize":16,"axes.labelsize":14,"legend.fontsize":10,"xtick.labelsize":10,"ytick.labelsize":10})
sns.set_style("ticks")
from matplotlib import rcParams
rcParams['font.family'] = 'serif'
heatmap_colors = 'Greens'
ref_color = '#000000'

# set up mappings
data = load(gopen('DATA.EXPERIMENTS.pkl.gz'))
default_vs_art = {0.125:'HIV_FAVITES_ART_0.125x', 0.25:'HIV_FAVITES_ART_0.25x', 0.5:'HIV_FAVITES_ART_0.5x', 1.:'HIV_FAVITES_DEFAULT', 2.:'HIV_FAVITES_ART_2x', 4.:'HIV_FAVITES_ART_4x', 8.:'HIV_FAVITES_ART_8x'}
ba_degree_02_vs_art = {0.125:'HIV_FAVITES_BA_EXPECTEDDEGREE_02_ART_0.125x', 0.25:'HIV_FAVITES_BA_EXPECTEDDEGREE_02_ART_0.25x', 0.5:'HIV_FAVITES_BA_EXPECTEDDEGREE_02_ART_0.5x', 1.:'HIV_FAVITES_BA_EXPECTEDDEGREE_02', 2.:'HIV_FAVITES_BA_EXPECTEDDEGREE_02_ART_2x', 4.:'HIV_FAVITES_BA_EXPECTEDDEGREE_02_ART_4x', 8.:'HIV_FAVITES_BA_EXPECTEDDEGREE_02_ART_8x'}
ba_degree_04_vs_art = default_vs_art
ba_degree_08_vs_art = {0.125:'HIV_FAVITES_BA_EXPECTEDDEGREE_08_ART_0.125x', 0.25:'HIV_FAVITES_BA_EXPECTEDDEGREE_08_ART_0.25x', 0.5:'HIV_FAVITES_BA_EXPECTEDDEGREE_08_ART_0.5x', 1.:'HIV_FAVITES_BA_EXPECTEDDEGREE_08', 2.:'HIV_FAVITES_BA_EXPECTEDDEGREE_08_ART_2x', 4.:'HIV_FAVITES_BA_EXPECTEDDEGREE_08_ART_4x', 8.:'HIV_FAVITES_BA_EXPECTEDDEGREE_08_ART_8x'}
ba_degree_16_vs_art = {0.125:'HIV_FAVITES_BA_EXPECTEDDEGREE_16_ART_0.125x', 0.25:'HIV_FAVITES_BA_EXPECTEDDEGREE_16_ART_0.25x', 0.5:'HIV_FAVITES_BA_EXPECTEDDEGREE_16_ART_0.5x', 1.:'HIV_FAVITES_BA_EXPECTEDDEGREE_16', 2.:'HIV_FAVITES_BA_EXPECTEDDEGREE_16_ART_2x', 4.:'HIV_FAVITES_BA_EXPECTEDDEGREE_16_ART_4x', 8.:'HIV_FAVITES_BA_EXPECTEDDEGREE_16_ART_8x'}
er_degree_02_vs_art = {0.125:'HIV_FAVITES_ER_EXPECTEDDEGREE_02_ART_0.125x', 0.25:'HIV_FAVITES_ER_EXPECTEDDEGREE_02_ART_0.25x', 0.5:'HIV_FAVITES_ER_EXPECTEDDEGREE_02_ART_0.5x', 1.:'HIV_FAVITES_ER_EXPECTEDDEGREE_02', 2.:'HIV_FAVITES_ER_EXPECTEDDEGREE_02_ART_2x', 4.:'HIV_FAVITES_ER_EXPECTEDDEGREE_02_ART_4x', 8.:'HIV_FAVITES_ER_EXPECTEDDEGREE_02_ART_8x'}
er_degree_04_vs_art = {0.125:'HIV_FAVITES_ER_ART_0.125x', 0.25:'HIV_FAVITES_ER_ART_0.25x', 0.5:'HIV_FAVITES_ER_ART_0.5x', 1.:'HIV_FAVITES_ER', 2.:'HIV_FAVITES_ER_ART_2x', 4.:'HIV_FAVITES_ER_ART_4x', 8.:'HIV_FAVITES_ER_ART_8x'}
er_degree_08_vs_art = {0.125:'HIV_FAVITES_ER_EXPECTEDDEGREE_08_ART_0.125x', 0.25:'HIV_FAVITES_ER_EXPECTEDDEGREE_08_ART_0.25x', 0.5:'HIV_FAVITES_ER_EXPECTEDDEGREE_08_ART_0.5x', 1.:'HIV_FAVITES_ER_EXPECTEDDEGREE_08', 2.:'HIV_FAVITES_ER_EXPECTEDDEGREE_08_ART_2x', 4.:'HIV_FAVITES_ER_EXPECTEDDEGREE_08_ART_4x', 8.:'HIV_FAVITES_ER_EXPECTEDDEGREE_08_ART_8x'}
er_degree_16_vs_art = {0.125:'HIV_FAVITES_ER_EXPECTEDDEGREE_16_ART_0.125x', 0.25:'HIV_FAVITES_ER_EXPECTEDDEGREE_16_ART_0.25x', 0.5:'HIV_FAVITES_ER_EXPECTEDDEGREE_16_ART_0.5x', 1.:'HIV_FAVITES_ER_EXPECTEDDEGREE_16', 2.:'HIV_FAVITES_ER_EXPECTEDDEGREE_16_ART_2x', 4.:'HIV_FAVITES_ER_EXPECTEDDEGREE_16_ART_4x', 8.:'HIV_FAVITES_ER_EXPECTEDDEGREE_16_ART_8x'}
ws_degree_02_vs_art = {0.125:'HIV_FAVITES_WS_EXPECTEDDEGREE_02_ART_0.125x', 0.25:'HIV_FAVITES_WS_EXPECTEDDEGREE_02_ART_0.25x', 0.5:'HIV_FAVITES_WS_EXPECTEDDEGREE_02_ART_0.5x', 1.:'HIV_FAVITES_WS_EXPECTEDDEGREE_02', 2.:'HIV_FAVITES_WS_EXPECTEDDEGREE_02_ART_2x', 4.:'HIV_FAVITES_WS_EXPECTEDDEGREE_02_ART_4x', 8.:'HIV_FAVITES_WS_EXPECTEDDEGREE_02_ART_8x'}
ws_degree_04_vs_art = {0.125:'HIV_FAVITES_WS_ART_0.125x', 0.25:'HIV_FAVITES_WS_ART_0.25x', 0.5:'HIV_FAVITES_WS_ART_0.5x', 1.:'HIV_FAVITES_ER', 2.:'HIV_FAVITES_WS_ART_2x', 4.:'HIV_FAVITES_WS_ART_4x', 8.:'HIV_FAVITES_WS_ART_8x'}
ws_degree_08_vs_art = {0.125:'HIV_FAVITES_WS_EXPECTEDDEGREE_08_ART_0.125x', 0.25:'HIV_FAVITES_WS_EXPECTEDDEGREE_08_ART_0.25x', 0.5:'HIV_FAVITES_WS_EXPECTEDDEGREE_08_ART_0.5x', 1.:'HIV_FAVITES_WS_EXPECTEDDEGREE_08', 2.:'HIV_FAVITES_WS_EXPECTEDDEGREE_08_ART_2x', 4.:'HIV_FAVITES_WS_EXPECTEDDEGREE_08_ART_4x', 8.:'HIV_FAVITES_WS_EXPECTEDDEGREE_08_ART_8x'}
ws_degree_16_vs_art = {0.125:'HIV_FAVITES_WS_EXPECTEDDEGREE_16_ART_0.125x', 0.25:'HIV_FAVITES_WS_EXPECTEDDEGREE_16_ART_0.25x', 0.5:'HIV_FAVITES_WS_EXPECTEDDEGREE_16_ART_0.5x', 1.:'HIV_FAVITES_WS_EXPECTEDDEGREE_16', 2.:'HIV_FAVITES_WS_EXPECTEDDEGREE_16_ART_2x', 4.:'HIV_FAVITES_WS_EXPECTEDDEGREE_16_ART_4x', 8.:'HIV_FAVITES_WS_EXPECTEDDEGREE_16_ART_8x'}
seedselection_edgeweighted_vs_art = {0.125:'HIV_FAVITES_SEEDSELECTION_EDGEWEIGHTED_ART_0.125x', 0.25:'HIV_FAVITES_SEEDSELECTION_EDGEWEIGHTED_ART_0.25x', 0.5:'HIV_FAVITES_SEEDSELECTION_EDGEWEIGHTED_ART_0.5x', 1.:'HIV_FAVITES_SEEDSELECTION_EDGEWEIGHTED', 2.:'HIV_FAVITES_SEEDSELECTION_EDGEWEIGHTED_ART_2x', 4.:'HIV_FAVITES_SEEDSELECTION_EDGEWEIGHTED_ART_4x', 8.:'HIV_FAVITES_SEEDSELECTION_EDGEWEIGHTED_ART_8x'}
art_vals = sorted(default_vs_art.keys())
art_x = []
for e in art_vals:
    art_x += [e]*10
art_vals_recip = [1./e for e in art_x]
degrees = [2,4,8,16]
degree2str = {2:'02',4:'04',8:'08',16:'16'}
degree_pal = {2:'#20641c', 4:'#6caed1', 8:'#2286ca', 16:'#bfe49e'}
degree_handles = [Patch(color=degree_pal[d],label="Expected Degree %d"%d) for d in degrees]
models = {'ba':'Barabási–Albert', 'er':'Erdős–Rényi', 'ws':'Watts-Strogatz'}
model_pal = sns.color_palette(n_colors=len(models))
model_pal = {m:model_pal[i] for i,m in enumerate(['ba','er','ws'])}
model_handles = [Patch(color=model_pal[m],label=models[m]) for m in models]
seedselection_random_vs_art = default_vs_art
seedselection = ['Random','Edge-Weighted']
seedselection_pal = sns.color_palette(n_colors=len(seedselection))
seedselection_pal = {seedselection[i]:seedselection_pal[i] for i in range(len(seedselection))}
seedselection_handles = [Patch(color=seedselection_pal[s],label="Seed Selection %s"%s) for s in seedselection]
num_infected_vs_100th_time_times = [10.*i/100 for i in range(101)]*10
art_vals_pal = sns.color_palette(palette="Set2",n_colors=len(art_vals))
art_vals_pal = {art_vals[i]:art_vals_pal[i] for i in range(len(art_vals))}
art_vals_handles = [Patch(color=art_vals_pal[art],label="Expected ART Time (years): %.3f" % (1./art)) for art in art_vals]
granich_states = ['S','AU','AT','CU','CT']
granich_state_to_name = {'S':'Susceptible', 'AU':'Acute Untreated', 'AT':'Acute Treated', 'CU':'Chronic Untreated', 'CT':'Chronic Treated'}
num_state_vs_100th_time_pal = sns.color_palette(palette="Set2",n_colors=len(granich_states))
num_state_vs_100th_time_pal = {granich_states[i]:num_state_vs_100th_time_pal[i] for i in range(len(granich_states))}
num_state_vs_100th_time_handles = [Patch(color=num_state_vs_100th_time_pal[s],label=granich_state_to_name[s]) for s in granich_states]
real_vs_sim_keys = ['LANL','SD','SIM','SIM_SEED','SIM_SAMPLE','SIM_FT','SIM_SUB']
real_vs_sim_pal = {'LANL':'#20641c', 'SD':'#6caed1', 'SIM':'#2286ca', 'SIM_FT':'#bfe49e', 'SIM_SEED':'#FF0000', 'SIM_SAMPLE':'#0000FF', 'SIM_SUB':'#bfe49e'}
real_vs_sim_handles = {'SD':Patch(color=real_vs_sim_pal['SD'],label="San Diego"), 'LANL':Patch(color=real_vs_sim_pal['LANL'],label="Los Alamos"), 'SIM':Patch(color=real_vs_sim_pal['SIM'],label="Simulated"), 'SIM_SEED':Patch(color=real_vs_sim_pal['SIM_SEED'],label="Simulated (Seeds)"), 'SIM_SUB':Patch(color=real_vs_sim_pal['SIM_SUB'],label="Simulated (Subsampled)"), 'SIM_SAMPLE':Patch(color=real_vs_sim_pal['SIM_SAMPLE'],label="Sampled from HMM"), 'SIM_FT':Patch(color=real_vs_sim_pal['SIM_FT'],label="Simulated (FastTree)")}
cluster_pal = {'CLUSTERPICKERII':{True:'#AAAAFF', False:'#0000FF'}, 'HIVTRACE':{True:'#FFAAAA', False:'#FF0000'}}
cluster_handles = [Patch(color=cluster_pal['CLUSTERPICKERII'][True],label="TreeCluster (With Singletons)"), Patch(color=cluster_pal['CLUSTERPICKERII'][False],label="TreeCluster (Without Singletons)"), Patch(color=cluster_pal['HIVTRACE'][True],label="HIV-TRACE (With Singletons)"), Patch(color=cluster_pal['HIVTRACE'][False],label="HIV-TRACE (Without Singletons)")]
scores = {} # scores[tool][expected_degree][exp_time_to_art][true_threshold][singletons][metric]
cn_model_pal = {'ba':'#0000FF', 'er':'#FF0000', 'ws':'#00FF00'}
cn_model_handles = [Patch(color=cn_model_pal['ba'],label='Barabási–Albert'),Patch(color=cn_model_pal['er'],label='Erdős–Rényi'),Patch(color=cn_model_pal['ws'],label='Watts-Strogatz')]
'''
### plot number of individuals in each state vs. time (default parameters) ###
fig = plt.figure()
for s in granich_states:
    ax = sns.pointplot(x=num_infected_vs_100th_time_times, y=data['HIV_FAVITES_DEFAULT']['num_state_vs_100th_time'][s], color=num_state_vs_100th_time_pal[s], markers='')
x_tick_int()
plt.title("Number of Individuals in Each State vs. Time")
plt.xlabel("Time (years)")
plt.ylabel("Number of Individuals")
plt.ylim(0,85000)
tick_labels = ax.xaxis.get_ticklabels()
for i in range(len(tick_labels)):
    if i % 10 != 0:
        tick_labels[i].set_visible(False)
legend = plt.legend(handles=num_state_vs_100th_time_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('num_state_vs_time.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

### plot number of individuals in each state vs. time (vary expected time to ART) ###
for s in granich_states:
    fig = plt.figure()
    for art in art_vals:
        ax = sns.pointplot(x=num_infected_vs_100th_time_times, y=data[ba_degree_04_vs_art[art]]['num_state_vs_100th_time'][s], color=art_vals_pal[art], markers='')
    x_tick_int()
    plt.title("Number of %s Individuals vs. Time"%s)
    plt.xlabel("Time (years)")
    plt.ylabel("Number of Individuals")
    tick_labels = ax.xaxis.get_ticklabels()
    for i in range(len(tick_labels)):
        if i % 10 != 0:
            tick_labels[i].set_visible(False)
    legend = plt.legend(handles=art_vals_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
    plt.show()
    fig.savefig('num_%s_vs_time_art.pdf'%s, format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
    plt.close()

### plot number of individuals in just CU and CT vs. time (vary expected time to ART) ###
cu_ct_linestyle = {'CU':'--','CT':'-'}
fig = plt.figure()
for s in ['CU','CT']:
    for art in art_vals:
        ax = sns.pointplot(x=num_infected_vs_100th_time_times, y=data[ba_degree_04_vs_art[art]]['num_state_vs_100th_time'][s], color=art_vals_pal[art], markers='', linestyles=cu_ct_linestyle[s], ci=None, scale=0.5)
x_tick_int()
plt.title("Number of CU and CT Individuals vs. Time")
plt.xlabel("Time (years)")
plt.ylabel("Number of Individuals")
tick_labels = ax.xaxis.get_ticklabels()
for i in range(len(tick_labels)):
    if i % 10 != 0:
        tick_labels[i].set_visible(False)
legend = plt.legend(handles=art_vals_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('num_cu_ct_vs_time_art.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()
'''
### plot number CU+CT vs. time (varying expected time to ART) ###
fig = plt.figure()
for art in art_vals:
    cu = data[ba_degree_04_vs_art[art]]['num_state_vs_100th_time']['CU']
    ct = data[ba_degree_04_vs_art[art]]['num_state_vs_100th_time']['CT']
    ax = sns.pointplot(x=num_infected_vs_100th_time_times, y=[cu[i]+ct[i] for i in range(len(data[ba_degree_04_vs_art[art]]['num_state_vs_100th_time']['CU']))], color=art_vals_pal[art], markers='', ci=None, scale=0.5)
x_tick_int()
plt.xlim(xmin=20)
plt.title("Number of Chronic (CU+CT) vs. Time")
plt.xlabel("Time (years")
plt.ylabel("Number of Chronic (CU+CT)")
tick_labels = ax.xaxis.get_ticklabels()
for i in range(len(tick_labels)):
    if i % 10 != 0:
        tick_labels[i].set_visible(False)
legend = plt.legend(handles=art_vals_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('cu_plus_ct_vs_time_art.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

### plot number (AU+CU)/(AT+CT) vs. time (vary expected time to ART) ###
ninety_ninety_ninety_color = '#0000FF'
fig = plt.figure()
for art in art_vals:
    au = data[ba_degree_04_vs_art[art]]['num_state_vs_100th_time']['AU']
    cu = data[ba_degree_04_vs_art[art]]['num_state_vs_100th_time']['CU']
    at = data[ba_degree_04_vs_art[art]]['num_state_vs_100th_time']['AT']
    ct = data[ba_degree_04_vs_art[art]]['num_state_vs_100th_time']['CT']
    ax = sns.pointplot(x=num_infected_vs_100th_time_times, y=[(au[i]+cu[i])/(at[i]+ct[i]) if at[i]+ct[i] > 0 else au[i]+cu[i] for i in range(len(data[ba_degree_04_vs_art[art]]['num_state_vs_100th_time']['CU']))], color=art_vals_pal[art], markers='', ci=None, scale=0.5)
x_tick_int()
plt.plot([-1000,1000],[1,1],linestyle='--',color=ref_color)
plt.plot([-1000,1000],[(1.-.9**3)/(.9**3),(1.-.9**3)/(.9**3)],linestyle='--',color=ninety_ninety_ninety_color)
ax.set_yscale('log')
plt.xlim(xmin=20)
plt.ylim(ymax=10)
plt.title("Untreated/Treated vs. Time")
plt.xlabel("Time (years)")
plt.ylabel("Untreated/Treated")
tick_labels = ax.xaxis.get_ticklabels()
for i in range(len(tick_labels)):
    if i % 10 != 0:
        tick_labels[i].set_visible(False)
legend = plt.legend(handles=art_vals_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('aucu_div_atct_vs_time_art.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

### plot number CU + CT + AU + AT vs. time (vary expected time to ART) ###
#cu_ct_linestyle = {'CU':'--','CT':'-'}
fig = plt.figure()
for art in art_vals:
    au = data[ba_degree_04_vs_art[art]]['num_state_vs_100th_time']['AU']
    cu = data[ba_degree_04_vs_art[art]]['num_state_vs_100th_time']['CU']
    at = data[ba_degree_04_vs_art[art]]['num_state_vs_100th_time']['AT']
    ct = data[ba_degree_04_vs_art[art]]['num_state_vs_100th_time']['CT']
    ax = sns.pointplot(x=num_infected_vs_100th_time_times, y=[au[i]+cu[i]+at[i]+ct[i] for i in range(len(cu))], color=art_vals_pal[art], markers='', ci=None, scale=0.5)
x_tick_int()
plt.xlim(xmin=20)
plt.ylim(ymin=15000)
plt.title("Total Infected vs. Time")
plt.xlabel("Time (years)")
plt.ylabel("Total Infected")
tick_labels = ax.xaxis.get_ticklabels()
for i in range(len(tick_labels)):
    if i % 10 != 0:
        tick_labels[i].set_visible(False)
legend = plt.legend(handles=art_vals_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('cuctauat_vs_time_art.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()
'''
### plot proportion of individuals in each state vs. time (vary expected time to ART) ###
for s in granich_states[1:]: # exclude susceptible
    fig = plt.figure()
    for art in art_vals:
        ax = sns.pointplot(x=num_infected_vs_100th_time_times, y=[data[ba_degree_04_vs_art[art]]['num_state_vs_100th_time'][s][i]/data[ba_degree_04_vs_art[art]]['num_infected'][int(i/101)] for i in range(len(data[ba_degree_04_vs_art[art]]['num_state_vs_100th_time'][s]))], color=art_vals_pal[art], markers='')
    x_tick_int()
    plt.title("Proportion of Infected Individuals in %s vs. Time"%s)
    plt.xlabel("Time (years)")
    plt.ylabel("Proportion of Infected Individuals")
    tick_labels = ax.xaxis.get_ticklabels()
    for i in range(len(tick_labels)):
        if i % 10 != 0:
            tick_labels[i].set_visible(False)
    legend = plt.legend(handles=art_vals_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
    plt.show()
    fig.savefig('prop_%s_vs_time_art.pdf'%s, format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
    plt.close()

### plot number of individuals in each state vs. time (vary expected degree) ###
for s in granich_states:
    fig = plt.figure()
    ax = sns.pointplot(x=num_infected_vs_100th_time_times, y=data[ba_degree_02_vs_art[1.]]['num_state_vs_100th_time'][s], color=degree_pal[2], markers='')
    sns.pointplot(x=num_infected_vs_100th_time_times, y=data[ba_degree_04_vs_art[1.]]['num_state_vs_100th_time'][s], color=degree_pal[4], markers='')
    sns.pointplot(x=num_infected_vs_100th_time_times, y=data[ba_degree_08_vs_art[1.]]['num_state_vs_100th_time'][s], color=degree_pal[8], markers='')
    sns.pointplot(x=num_infected_vs_100th_time_times, y=data[ba_degree_16_vs_art[1.]]['num_state_vs_100th_time'][s], color=degree_pal[16], markers='')
    x_tick_int()
    plt.title("Number of %s Individuals vs. Time"%s)
    plt.xlabel("Time (years)")
    plt.ylabel("Number of Individuals")
    tick_labels = ax.xaxis.get_ticklabels()
    for i in range(len(tick_labels)):
        if i % 10 != 0:
            tick_labels[i].set_visible(False)
    legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
    plt.show()
    fig.savefig('num_%s_vs_time_degree.pdf'%s, format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
    plt.close()

### plot number of individuals in just CU and CT vs. time (vary expected degree) ###
cu_ct_linestyle = {'CU':'--','CT':'-'}
fig = plt.figure()
for s in ['CU','CT']:
    ax = sns.pointplot(x=num_infected_vs_100th_time_times, y=data[ba_degree_02_vs_art[1.]]['num_state_vs_100th_time'][s], color=degree_pal[2], markers='', linestyles=cu_ct_linestyle[s], ci=None, scale=0.5)
    sns.pointplot(x=num_infected_vs_100th_time_times, y=data[ba_degree_04_vs_art[1.]]['num_state_vs_100th_time'][s], color=degree_pal[4], markers='', linestyles=cu_ct_linestyle[s], ci=None, scale=0.5)
    sns.pointplot(x=num_infected_vs_100th_time_times, y=data[ba_degree_08_vs_art[1.]]['num_state_vs_100th_time'][s], color=degree_pal[8], markers='', linestyles=cu_ct_linestyle[s], ci=None, scale=0.5)
    sns.pointplot(x=num_infected_vs_100th_time_times, y=data[ba_degree_16_vs_art[1.]]['num_state_vs_100th_time'][s], color=degree_pal[16], markers='', linestyles=cu_ct_linestyle[s], ci=None, scale=0.5)
    x_tick_int()
plt.title("Number of CU and CT Individuals vs. Time")
plt.xlabel("Time (years)")
plt.ylabel("Number of Individuals")
tick_labels = ax.xaxis.get_ticklabels()
for i in range(len(tick_labels)):
    if i % 10 != 0:
        tick_labels[i].set_visible(False)
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('num_cu_ct_vs_time_degree.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

### plot number of individuals in each infected state vs. time (default parameters) ###
fig = plt.figure()
for s in granich_states[1:]:
    ax = sns.pointplot(x=num_infected_vs_100th_time_times, y=data['HIV_FAVITES_DEFAULT']['num_state_vs_100th_time'][s], color=num_state_vs_100th_time_pal[s], markers='')
x_tick_int()
plt.title("Number of Individuals in Each State vs. Time")
plt.xlabel("Time (years)")
plt.ylabel("Number of Individuals")
plt.ylim(0,15000)
tick_labels = ax.xaxis.get_ticklabels()
for i in range(len(tick_labels)):
    if i % 10 != 0:
        tick_labels[i].set_visible(False)
legend = plt.legend(handles=num_state_vs_100th_time_handles[1:],bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('num_infected_state_vs_time.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()
'''
### plot KDE of branch lengths of real and simulated trees ###
fig = plt.figure()
keys = ['LANL','SD','SIM','SIM_SUB']#,'SIM_FT']
for k in keys:
    sns.distplot(load(gopen('DATA.BRANCH_LENGTHS.%s.pkl.gz' % k)), kde=True, hist=False, color=real_vs_sim_pal[k])
plt.title("Branch Length Distributions")
plt.xlabel("Branch Length")
plt.ylabel("Kernel Density Estimate")
plt.xlim(xmin=0,xmax=0.06)
legend = plt.legend(handles=[real_vs_sim_handles[k] for k in keys],bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
w,h = fig.get_size_inches()
fig.set_size_inches(w, h*0.6)
plt.show()
fig.savefig('real_vs_sim_branch_lengths.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

### plot KDE of pairwise TN93 sequence distances ###
fig = plt.figure()
keys = ['LANL','SD','SIM_SUB']
for k in keys:
    sns.distplot(load(gopen('DATA.PAIR_DISTS.TN93.%s.pkl.gz' % k)), kde=True, hist=False, color=real_vs_sim_pal[k])
plt.title("TN93 Distance Distributions")
plt.xlabel("Pairwise TN93 Distance")
plt.ylabel("Kernel Density Estimate")
plt.xlim(xmin=0)
legend = plt.legend(handles=[real_vs_sim_handles[k] for k in keys],bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
w,h = fig.get_size_inches()
fig.set_size_inches(w, h*0.6)
plt.show()
fig.savefig('real_vs_sim_pair-dists_tn93.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()
'''
### plot avg. number of infections after year 8 or 9 vs. ART ###
linestyles = {'All':'--', 'Top':'-'}
ymax = 0.4
for y in [8,9]:
    for tool in ['hivtrace','treecluster']:
        # BA model
        fig = plt.figure()
        for g in ['All','Top']:
            ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_02_vs_art[x]]['avg_infections_%s_after_time_%d_%s' % (g,y,tool)]], color=degree_pal[2], linestyles=linestyles[g])
            sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_04_vs_art[x]]['avg_infections_%s_after_time_%d_%s' % (g,y,tool)]], color=degree_pal[4], linestyles=linestyles[g])
            sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_08_vs_art[x]]['avg_infections_%s_after_time_%d_%s' % (g,y,tool)]], color=degree_pal[8], linestyles=linestyles[g])
            sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_16_vs_art[x]]['avg_infections_%s_after_time_%d_%s' % (g,y,tool)]], color=degree_pal[16], linestyles=linestyles[g])
        ax.invert_xaxis()
        tool_name = {'treecluster':'TreeCluster','hivtrace':'HIV-TRACE'}[tool]
        plt.title("BA Average Number of Infections (Year %d to End) (%s) vs. ART"%(y,tool_name))
        plt.xlabel("Expected Time to Begin ART (years)")
        plt.ylabel("BA Average Number of Infections (Year %d to End) (%s)"%(y,tool_name))
        plt.ylim(0,ymax)
        legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
        plt.show()
        fig.savefig('ba_avg_infections_after_time_%d_%s.pdf' % (y,tool), format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
        plt.close()

        # ER model
        fig = plt.figure()
        for g in ['All','Top']:
            ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_02_vs_art[x]]['avg_infections_%s_after_time_%d_%s' % (g,y,tool)]], color=degree_pal[2], linestyles=linestyles[g])
            sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_04_vs_art[x]]['avg_infections_%s_after_time_%d_%s' % (g,y,tool)]], color=degree_pal[4], linestyles=linestyles[g])
            sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_08_vs_art[x]]['avg_infections_%s_after_time_%d_%s' % (g,y,tool)]], color=degree_pal[8], linestyles=linestyles[g])
            sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_16_vs_art[x]]['avg_infections_%s_after_time_%d_%s' % (g,y,tool)]], color=degree_pal[16], linestyles=linestyles[g])
        ax.invert_xaxis()
        tool_name = {'treecluster':'TreeCluster','hivtrace':'HIV-TRACE'}[tool]
        plt.title("ER Average Number of Infections (Year %d to End) (%s) vs. ART"%(y,tool_name))
        plt.xlabel("Expected Time to Begin ART (years)")
        plt.ylabel("ER Average Number of Infections (Year %d to End) (%s)"%(y,tool_name))
        plt.ylim(0,ymax)
        legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
        plt.show()
        fig.savefig('er_avg_infections_after_time_%d_%s.pdf' % (y,tool), format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
        plt.close()

        # WS model
        fig = plt.figure()
        for g in ['All','Top']:
            ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_02_vs_art[x]]['avg_infections_%s_after_time_%d_%s' % (g,y,tool)]], color=degree_pal[2], linestyles=linestyles[g])
            sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_04_vs_art[x]]['avg_infections_%s_after_time_%d_%s' % (g,y,tool)]], color=degree_pal[4], linestyles=linestyles[g])
            sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_08_vs_art[x]]['avg_infections_%s_after_time_%d_%s' % (g,y,tool)]], color=degree_pal[8], linestyles=linestyles[g])
            sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_16_vs_art[x]]['avg_infections_%s_after_time_%d_%s' % (g,y,tool)]], color=degree_pal[16], linestyles=linestyles[g])
        ax.invert_xaxis()
        tool_name = {'treecluster':'TreeCluster','hivtrace':'HIV-TRACE'}[tool]
        plt.title("WS Average Number of Infections (Year %d to End) (%s) vs. ART"%(y,tool_name))
        plt.xlabel("Expected Time to Begin ART (years)")
        plt.ylabel("WS Average Number of Infections (Year %d to End) (%s)"%(y,tool_name))
        plt.ylim(0,ymax)
        legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
        plt.show()
        fig.savefig('ws_avg_infections_after_time_%d_%s.pdf' % (y,tool), format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
        plt.close()

        # seed selection
        fig = plt.figure()
        for g in ['All','Top']:
            ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_random_vs_art[x]]['avg_infections_%s_after_time_%d_%s' % (g,y,tool)]], color=seedselection_pal["Random"], linestyles=linestyles[g])
            sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_edgeweighted_vs_art[x]]['avg_infections_%s_after_time_%d_%s' % (g,y,tool)]], color=seedselection_pal["Edge-Weighted"], linestyles=linestyles[g])
        ax.invert_xaxis()
        plt.title("Seed Selection Average Number of Infections (Year %d to End) (%s) vs. ART"%(y,tool_name))
        plt.xlabel("Expected Time to Begin ART (years)")
        plt.ylabel("Seed Selection Average Number of Infections (Year %d to End) (%s)"%(y,tool_name))
        plt.ylim(0,ymax)
        plt.plot([-1000,1000],[15000,15000],linestyle='--',color=ref_color)
        legend = plt.legend(handles=seedselection_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
        plt.show()
        fig.savefig('seedselection_avg_infections_after_time_%d_%s.pdf' % (y,tool), format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
        plt.close()

### plot avg. number of infections after year 9 (growthrate sorted) vs. ART ###
linestyles = {'All':'--', 'Top':'-'}
ymax = 0.4
for tool in ['hivtrace','treecluster']:
    # BA model
    fig = plt.figure()
    for g in ['All','Top']:
        ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_02_vs_art[x]]['avg_infections_%s_after_time_9_growthsorted_%s' % (g,tool)]], color=degree_pal[2], linestyles=linestyles[g])
        sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_04_vs_art[x]]['avg_infections_%s_after_time_9_growthsorted_%s' % (g,tool)]], color=degree_pal[4], linestyles=linestyles[g])
        sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_08_vs_art[x]]['avg_infections_%s_after_time_9_growthsorted_%s' % (g,tool)]], color=degree_pal[8], linestyles=linestyles[g])
        sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_16_vs_art[x]]['avg_infections_%s_after_time_9_growthsorted_%s' % (g,tool)]], color=degree_pal[16], linestyles=linestyles[g])
    ax.invert_xaxis()
    tool_name = {'treecluster':'TreeCluster','hivtrace':'HIV-TRACE'}[tool]
    plt.title("BA Average Number of Infections (Growth-Sorted) (%s) vs. ART"%tool_name)
    plt.xlabel("Expected Time to Begin ART (years)")
    plt.ylabel("BA Average Number of Infections (Growth-Sorted) (%s)"%tool_name)
    plt.ylim(0,ymax)
    legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
    plt.show()
    fig.savefig('ba_avg_infections_after_time_9_growthsorted_%s.pdf' % tool, format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
    plt.close()

    # ER model
    fig = plt.figure()
    for g in ['All','Top']:
        ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_02_vs_art[x]]['avg_infections_%s_after_time_9_growthsorted_%s' % (g,tool)]], color=degree_pal[2], linestyles=linestyles[g])
        sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_04_vs_art[x]]['avg_infections_%s_after_time_9_growthsorted_%s' % (g,tool)]], color=degree_pal[4], linestyles=linestyles[g])
        sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_08_vs_art[x]]['avg_infections_%s_after_time_9_growthsorted_%s' % (g,tool)]], color=degree_pal[8], linestyles=linestyles[g])
        sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_16_vs_art[x]]['avg_infections_%s_after_time_9_growthsorted_%s' % (g,tool)]], color=degree_pal[16], linestyles=linestyles[g])
    ax.invert_xaxis()
    tool_name = {'treecluster':'TreeCluster','hivtrace':'HIV-TRACE'}[tool]
    plt.title("ER Average Number of Infections (Growth-Sorted) (%s) vs. ART"%tool_name)
    plt.xlabel("Expected Time to Begin ART (years)")
    plt.ylabel("ER Average Number of Infections (Growth-Sorted) (%s)"%tool_name)
    plt.ylim(0,ymax)
    legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
    plt.show()
    fig.savefig('er_avg_infections_after_time_9_growthsorted_%s.pdf' % tool, format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
    plt.close()

    # WS model
    fig = plt.figure()
    for g in ['All','Top']:
        ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_02_vs_art[x]]['avg_infections_%s_after_time_9_growthsorted_%s' % (g,tool)]], color=degree_pal[2], linestyles=linestyles[g])
        sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_04_vs_art[x]]['avg_infections_%s_after_time_9_growthsorted_%s' % (g,tool)]], color=degree_pal[4], linestyles=linestyles[g])
        sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_08_vs_art[x]]['avg_infections_%s_after_time_9_growthsorted_%s' % (g,tool)]], color=degree_pal[8], linestyles=linestyles[g])
        sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_16_vs_art[x]]['avg_infections_%s_after_time_9_growthsorted_%s' % (g,tool)]], color=degree_pal[16], linestyles=linestyles[g])
    ax.invert_xaxis()
    tool_name = {'treecluster':'TreeCluster','hivtrace':'HIV-TRACE'}[tool]
    plt.title("WS Average Number of Infections (Growth-Sorted) (%s) vs. ART"%tool_name)
    plt.xlabel("Expected Time to Begin ART (years)")
    plt.ylabel("WS Average Number of Infections (Growth-Sorted) (%s)"%tool_name)
    plt.ylim(0,ymax)
    legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
    plt.show()
    fig.savefig('ws_avg_infections_after_time_9_growthsorted_%s.pdf' % tool, format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
    plt.close()

    # seed selection
    fig = plt.figure()
    for g in ['All','Top']:
        ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_random_vs_art[x]]['avg_infections_%s_after_time_9_growthsorted_%s' % (g,tool)]], color=seedselection_pal["Random"], linestyles=linestyles[g])
        sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_edgeweighted_vs_art[x]]['avg_infections_%s_after_time_9_growthsorted_%s' % (g,tool)]], color=seedselection_pal["Edge-Weighted"], linestyles=linestyles[g])
    ax.invert_xaxis()
    plt.title("Seed Selection Average Number of Infections (Growth-Sorted) (%s) vs. ART"%tool_name)
    plt.xlabel("Expected Time to Begin ART (years)")
    plt.ylabel("Seed Selection Average Number of Infections (Growth-Sorted) (%s)"%tool_name)
    plt.ylim(0,ymax)
    plt.plot([-1000,1000],[15000,15000],linestyle='--',color=ref_color)
    legend = plt.legend(handles=seedselection_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
    plt.show()
    fig.savefig('seedselection_avg_infections_after_time_9_growthsorted_%s.pdf' % tool, format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
    plt.close()
'''
### plot avg. number of infections after year 9 (growthrate sorted 1000 top) vs. ART ###
ymax = 0.2
top_1000_colors = {'all':'#20641c', 'hivtrace':'#6caed1', 'treecluster':'#2286ca'}#, 'treecluster_argmaxclusters':'#bfe49e'}
top_1000_translate = {'all':'Expected','hivtrace':'HIV-TRACE','treecluster':'TreeCluster'}#,'treecluster_argmaxclusters':'TreeCluster (Argmax Clusters)'}
top_1000_handles = [Patch(color=top_1000_colors[k],label=top_1000_translate[k]) for k in sorted(top_1000_colors.keys())]
cluster_handles = [Patch(color=cluster_pal['CLUSTERPICKERII'][True],label="TreeCluster (With Singletons)"), Patch(color=cluster_pal['CLUSTERPICKERII'][False],label="TreeCluster (Without Singletons)"), Patch(color=cluster_pal['HIVTRACE'][True],label="HIV-TRACE (With Singletons)"), Patch(color=cluster_pal['HIVTRACE'][False],label="HIV-TRACE (Without Singletons)")]
for num_top in [200,1000,5000]:
    for degree in ['02','04','08','16']:
        # BA model
        fig = plt.figure()
        ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[globals()['ba_degree_%s_vs_art'%degree][x]]['avg_infections_All_after_time_9_growthsorted_hivtrace']], color=top_1000_colors['all'])
        for tool in ['hivtrace','treecluster']:#,'treecluster_argmaxclusters']:
            sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[globals()['ba_degree_%s_vs_art'%degree][x]]['avg_infections_Top_after_time_9_growthsorted_%dtop_%s' % (num_top,tool)]], color=top_1000_colors[tool])
        ax.invert_xaxis()
        tool_name = {'treecluster':'TreeCluster','hivtrace':'HIV-TRACE','treecluster_argmaxclusters':'TreeCluster (Argmax Clusters)'}[tool]
        plt.title("Efficacy (Top %d, Exp. Deg. %d)"%(num_top,int(degree)))
        plt.xlabel("Expected Time to Begin ART (years)")
        plt.ylabel("Average Number of Infections")
        plt.ylim(0,ymax)
        legend = plt.legend(handles=top_1000_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
        plt.show()
        fig.savefig('ba_avg_infections_after_time_9_growthsorted_degree_%s_top_%d.pdf'%(degree,num_top), format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
        plt.close()
'''
        # ER model
        fig = plt.figure()
        ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[globals()['er_degree_%s_vs_art'%degree][x]]['avg_infections_All_after_time_9_growthsorted_hivtrace']], color=top_1000_colors['all'])
        for tool in ['hivtrace','treecluster','treecluster_argmaxclusters']:
            sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[globals()['er_degree_%s_vs_art'%degree][x]]['avg_infections_Top_after_time_9_growthsorted_%dtop_%s' % (num_top,tool)]], color=top_1000_colors[tool])
        ax.invert_xaxis()
        tool_name = {'treecluster':'TreeCluster','hivtrace':'HIV-TRACE','treecluster_argmaxclusters':'TreeCluster (Argmax Clusters)'}[tool]
        plt.title("ER Avg. Num. Inf. vs. ART (Top %d, Exp. Deg. %d)"%(num_top,int(degree)))
        plt.xlabel("Expected Time to Begin ART (years)")
        plt.ylabel("ER Average Number of Infections")
        plt.ylim(0,ymax)
        legend = plt.legend(handles=top_1000_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
        plt.show()
        fig.savefig('er_avg_infections_after_time_9_growthsorted_degree_%s_top_%d.pdf'%(degree,num_top), format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
        plt.close()

        # WS model
        fig = plt.figure()
        ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[globals()['ws_degree_%s_vs_art'%degree][x]]['avg_infections_All_after_time_9_growthsorted_hivtrace']], color=top_1000_colors['all'])
        for tool in ['hivtrace','treecluster','treecluster_argmaxclusters']:
            sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[globals()['ws_degree_%s_vs_art'%degree][x]]['avg_infections_Top_after_time_9_growthsorted_%dtop_%s' % (num_top,tool)]], color=top_1000_colors[tool])
        ax.invert_xaxis()
        tool_name = {'treecluster':'TreeCluster','hivtrace':'HIV-TRACE','treecluster_argmaxclusters':'TreeCluster (Argmax Clusters)'}[tool]
        plt.title("WS Avg. Num. Inf. vs. ART (Top %d, Exp. Deg. %d)"%(num_top,int(degree)))
        plt.xlabel("Expected Time to Begin ART (years)")
        plt.ylabel("WS Average Number of Infections")
        plt.ylim(0,ymax)
        legend = plt.legend(handles=top_1000_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
        plt.show()
        fig.savefig('ws_avg_infections_after_time_9_growthsorted_degree_%s_top_%d.pdf'%(degree,num_top), format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
        plt.close()
'''
### plot num_infected vs. ART ###
# BA model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_02_vs_art[x]]['num_infected']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_04_vs_art[x]]['num_infected']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_08_vs_art[x]]['num_infected']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_16_vs_art[x]]['num_infected']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("Number Infected vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("Number of Infected Individuals")
plt.ylim(0,100000)
plt.plot([-1000,1000],[15000,15000],linestyle='--',color=ref_color)
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('ba_num_infected.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()
'''
# ER model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_02_vs_art[x]]['num_infected']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_04_vs_art[x]]['num_infected']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_08_vs_art[x]]['num_infected']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_16_vs_art[x]]['num_infected']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("ER Num. Infected Individuals vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("ER Number of Infected Individuals (100,000 total)")
plt.ylim(0,100000)
plt.plot([-1000,1000],[15000,15000],linestyle='--',color=ref_color)
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('er_num_infected.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# WS model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_02_vs_art[x]]['num_infected']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_04_vs_art[x]]['num_infected']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_08_vs_art[x]]['num_infected']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_16_vs_art[x]]['num_infected']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("WS Num. Infected Individuals vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("WS Number of Infected Individuals (100,000 total)")
plt.ylim(0,100000)
plt.plot([-1000,1000],[15000,15000],linestyle='--',color=ref_color)
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('ws_num_infected.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# seed selection
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_random_vs_art[x]]['num_infected']], color=seedselection_pal["Random"])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_edgeweighted_vs_art[x]]['num_infected']], color=seedselection_pal["Edge-Weighted"])
ax.invert_xaxis()
plt.title("Seed Selection Num. Infected Individuals vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("Seed Selection Number of Infected Individuals (100,000 total)")
plt.ylim(0,100000)
plt.plot([-1000,1000],[15000,15000],linestyle='--',color=ref_color)
legend = plt.legend(handles=seedselection_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('seedselection_num_infected.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()
'''
'''
# have figures be expected degree and curves be models instead
for degree in ['02','04','08','16']:
    fig = plt.figure()
    for model in ['ba','er','ws']:
        ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[globals()['%s_degree_%s_vs_art'%(model,degree)][x]]['num_infected']], color=model_pal[model])
    ax.invert_xaxis()
    plt.title("Num. Infected vs. ART (Exp. Degree %d)"%int(degree))
    plt.xlabel("Expeced Time to Begin ART (years)")
    plt.ylabel("Number of Infected Individuals (100,000 total)")
    plt.ylim(0,100000)
    plt.plot([-1000,1000],[15000,15000],linestyle='--',color=ref_color)
    legend = plt.legend(handles=model_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
    plt.show()
    fig.savefig('num_infected_degree_%s.pdf'%degree, format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
    plt.close()
'''
### plot average branch length vs. ART ###
# BA model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_02_vs_art[x]]['avg_bl']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_04_vs_art[x]]['avg_bl']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_08_vs_art[x]]['avg_bl']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_16_vs_art[x]]['avg_bl']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("Average Branch Length vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("Average Branch Length")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('ba_avg_bl.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()
'''
# ER model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_02_vs_art[x]]['avg_bl']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_04_vs_art[x]]['avg_bl']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_08_vs_art[x]]['avg_bl']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_16_vs_art[x]]['avg_bl']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("ER Average Branch Length vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("ER Average Branch Length")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('er_avg_bl.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# WS model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_02_vs_art[x]]['avg_bl']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_04_vs_art[x]]['avg_bl']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_08_vs_art[x]]['avg_bl']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_16_vs_art[x]]['avg_bl']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("WS Average Branch Length vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("WS Average Branch Length")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('ws_avg_bl.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# seed selection
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_random_vs_art[x]]['avg_bl']], color=seedselection_pal["Random"])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_edgeweighted_vs_art[x]]['avg_bl']], color=seedselection_pal["Edge-Weighted"])
ax.invert_xaxis()
plt.title("Seed Selection Average Branch Length vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("Average Branch Length")
legend = plt.legend(handles=seedselection_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('seedselection_avg_bl.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

### plot FastTree RF distance vs. ART ###
# BA model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_02_vs_art[x]]['FASTTREE_RF']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_04_vs_art[x]]['FASTTREE_RF']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_08_vs_art[x]]['FASTTREE_RF']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_16_vs_art[x]]['FASTTREE_RF']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("FastTree RF Distance vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("FastTree RF Distance")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('ba_fasttree_rf.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# ER model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_02_vs_art[x]]['FASTTREE_RF']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_04_vs_art[x]]['FASTTREE_RF']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_08_vs_art[x]]['FASTTREE_RF']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_16_vs_art[x]]['FASTTREE_RF']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("ER FastTree RF Distance vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("ER FastTree RF Distance")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('er_fasttree_rf.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# WS model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_02_vs_art[x]]['FASTTREE_RF']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_04_vs_art[x]]['FASTTREE_RF']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_08_vs_art[x]]['FASTTREE_RF']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_16_vs_art[x]]['FASTTREE_RF']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("WS FastTree RF Distance vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("WS FastTree RF Distance")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('ws_fasttree_rf.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# seed selection
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_random_vs_art[x]]['FASTTREE_RF']], color=seedselection_pal["Random"])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_edgeweighted_vs_art[x]]['FASTTREE_RF']], color=seedselection_pal["Edge-Weighted"])
ax.invert_xaxis()
plt.title("Seed Selection FastTree RF Distance vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("FastTree RF Distance")
legend = plt.legend(handles=seedselection_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('seedselection_fasttree_rf.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

### plot FastTree Quartet distance vs. ART ###
# BA model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_02_vs_art[x]]['FASTTREE_QUARTET_DIST']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_04_vs_art[x]]['FASTTREE_QUARTET_DIST']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_08_vs_art[x]]['FASTTREE_QUARTET_DIST']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_16_vs_art[x]]['FASTTREE_QUARTET_DIST']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("BA FastTree Quartet Distance vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("BA FastTree Quartet Distance")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('ba_fasttree_quartet.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# ER model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_02_vs_art[x]]['FASTTREE_QUARTET_DIST']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_04_vs_art[x]]['FASTTREE_QUARTET_DIST']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_08_vs_art[x]]['FASTTREE_QUARTET_DIST']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_16_vs_art[x]]['FASTTREE_QUARTET_DIST']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("ER FastTree Quartet Distance vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("ER FastTree Quartet Distance")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('er_fasttree_quartet.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# WS model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_02_vs_art[x]]['FASTTREE_QUARTET_DIST']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_04_vs_art[x]]['FASTTREE_QUARTET_DIST']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_08_vs_art[x]]['FASTTREE_QUARTET_DIST']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_16_vs_art[x]]['FASTTREE_QUARTET_DIST']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("WS FastTree Quartet Distance vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("WS FastTree Quartet Distance")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('ws_fasttree_quartet.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# seed selection
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_random_vs_art[x]]['FASTTREE_QUARTET_DIST']], color=seedselection_pal["Random"])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_edgeweighted_vs_art[x]]['FASTTREE_QUARTET_DIST']], color=seedselection_pal["Edge-Weighted"])
ax.invert_xaxis()
plt.title("Seed Selection FastTree Quartet Distance vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("FastTree Quartet Distance")
legend = plt.legend(handles=seedselection_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('seedselection_fasttree_quartet.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

### plot cherry fraction vs. ART ###
# BA model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_02_vs_art[x]]['cherry_fraction_true']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_04_vs_art[x]]['cherry_fraction_true']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_08_vs_art[x]]['cherry_fraction_true']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_16_vs_art[x]]['cherry_fraction_true']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("BA Cherry Fraction vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("BA Cherry Fraction")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('ba_cherry_fraction_true.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# ER model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_02_vs_art[x]]['cherry_fraction_true']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_04_vs_art[x]]['cherry_fraction_true']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_08_vs_art[x]]['cherry_fraction_true']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_16_vs_art[x]]['cherry_fraction_true']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("ER Cherry Fraction vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("ER Cherry Fraction")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('er_cherry_fraction_true.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# WS model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_02_vs_art[x]]['cherry_fraction_true']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_04_vs_art[x]]['cherry_fraction_true']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_08_vs_art[x]]['cherry_fraction_true']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_16_vs_art[x]]['cherry_fraction_true']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("WS Cherry Fraction vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("WS Cherry Fraction")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('ws_cherry_fraction_true.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# seed selection
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_random_vs_art[x]]['cherry_fraction_true']], color=seedselection_pal["Random"])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_edgeweighted_vs_art[x]]['cherry_fraction_true']], color=seedselection_pal["Edge-Weighted"])
ax.invert_xaxis()
plt.title("Seed Selection Cherry Fraction vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("Cherry Fraction")
legend = plt.legend(handles=seedselection_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('seedselection_cherry_fraction_true.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

### plot proportion of branches with <= 1 expected mutation vs. ART ###
# BA model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_02_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_04_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_08_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_16_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("BA Proportion Branches ≤ 1 Exp. Mutation vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("BA Proportion Branches ≤ 1 Exp. Mutation")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('ba_prop_branch_lte_1_exp_mut.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# ER model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_02_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_04_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_08_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_16_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("ER Proportion Branches ≤ 1 Exp. Mutation vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("ER Proportion Branches ≤ 1 Exp. Mutation")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('er_prop_branch_lte_1_exp_mut.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# WS model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_02_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_04_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_08_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_16_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[16])
ax.invert_xaxis()
plt.title("WS Proportion Branches ≤ 1 Exp. Mutation vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("WS Proportion Branches ≤ 1 Exp. Mutation")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('ws_prop_branch_lte_1_exp_mut.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# seed selection
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_random_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=seedselection_pal["Random"])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_edgeweighted_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=seedselection_pal["Edge-Weighted"])
ax.invert_xaxis()
plt.title("Seed Selection Proportion Branches ≤ 1 Exp. Mutation vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
plt.ylabel("Proportion Branches ≤ 1 Exp. Mutation")
legend = plt.legend(handles=seedselection_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('seedselection_prop_branch_lte_1_exp_mut.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()
'''
### plot RF distance with proportion of branches with <= expected mutation vs. ART ###
# BA model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_02_vs_art[x]]['FASTTREE_RF']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_04_vs_art[x]]['FASTTREE_RF']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_08_vs_art[x]]['FASTTREE_RF']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_16_vs_art[x]]['FASTTREE_RF']], color=degree_pal[16])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_02_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[2], linestyles=':')
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_04_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[4], linestyles=':')
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_08_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[8], linestyles=':')
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ba_degree_16_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[16], linestyles=':')
ax.invert_xaxis()
plt.title("RF Dist., Proportion Short Branches vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('ba_rf_and_prop_branch_lte_1_exp_mut.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()
'''
# ER model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_02_vs_art[x]]['FASTTREE_RF']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_04_vs_art[x]]['FASTTREE_RF']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_08_vs_art[x]]['FASTTREE_RF']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_16_vs_art[x]]['FASTTREE_RF']], color=degree_pal[16])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_02_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[2], linestyles=':')
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_04_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[4], linestyles=':')
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_08_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[8], linestyles=':')
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[er_degree_16_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[16], linestyles=':')
ax.invert_xaxis()
plt.title("ER RF Distance and Proportion of Short Branches vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('er_rf_and_prop_branch_lte_1_exp_mut.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# WS model
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_02_vs_art[x]]['FASTTREE_RF']], color=degree_pal[2])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_04_vs_art[x]]['FASTTREE_RF']], color=degree_pal[4])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_08_vs_art[x]]['FASTTREE_RF']], color=degree_pal[8])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_16_vs_art[x]]['FASTTREE_RF']], color=degree_pal[16])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_02_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[2], linestyles=':')
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_04_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[4], linestyles=':')
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_08_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[8], linestyles=':')
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[ws_degree_16_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=degree_pal[16], linestyles=':')
ax.invert_xaxis()
plt.title("WS RF Distance and Proportion of Short Branches vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
legend = plt.legend(handles=degree_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('ws_rf_and_prop_branch_lte_1_exp_mut.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()

# seed selection
fig = plt.figure()
ax = sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_random_vs_art[x]]['FASTTREE_RF']], color=seedselection_pal["Random"])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_edgeweighted_vs_art[x]]['FASTTREE_RF']], color=seedselection_pal["Edge-Weighted"])
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_random_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=seedselection_pal["Random"], linestyles=':')
sns.pointplot(x=art_vals_recip, y=[val for x in art_vals for val in data[seedselection_edgeweighted_vs_art[x]]['prop_branch_lte_1_exp_mut']], color=seedselection_pal["Edge-Weighted"], linestyles=':')
ax.invert_xaxis()
plt.title("Seed Selection RF Distance and Proportion of Short Branches vs. ART")
plt.xlabel("Expected Time to Begin ART (years)")
legend = plt.legend(handles=seedselection_handles,bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., frameon=True)
plt.show()
fig.savefig('seedselection_rf_and_prop_branch_lte_1_exp_mut.pdf', format='pdf', bbox_extra_artists=(legend,), bbox_inches='tight')
plt.close()
'''
### plot proportion of infected people that are true singletons ###
NUM_EXP_DEGREES = len(degrees)
EXP_TIME_ART = [1./art_rate for art_rate in sorted(default_vs_art.keys())]
EXP_TIME_ART_STR = [r'$\frac{1}{%d}$'%int(1./e) if e < 1 else str(int(e))  for e in EXP_TIME_ART]
NUM_EXP_TIME_ART = len(EXP_TIME_ART)
THRESHOLDS = [t for t in sorted({float(k.split('_')[3]) for k in data['HIV_FAVITES_DEFAULT'].keys() if 'LENGTHCLADE' in k and 'NOSINGLETONS' not in k})]
NUM_THRESHOLDS = len(THRESHOLDS)
fig, axes = plt.subplots(NUM_THRESHOLDS, NUM_EXP_DEGREES, sharex=True, sharey=True, figsize=(14, 16))
for row in range(NUM_THRESHOLDS):
    t = THRESHOLDS[row]
    for col in range(NUM_EXP_DEGREES):
        exp_degree = degrees[col]
        for m in ('ba','er','ws'):
            num_singletons = {d:{1./art_rate:{int(k.split('_')[-1]):v for k,v in data[globals()['%s_degree_%s_vs_art' % (m,degree2str[d])][art_rate]].items() if k.startswith('NUM_SINGLETONS_LENGTHCLADE_')} for art_rate in sorted(globals()['ba_degree_%s_vs_art' % degree2str[d]].keys())} for d in degrees}
            num_infected = {d:{1./art_rate:data[globals()['%s_degree_%s_vs_art' % (m,degree2str[d])][art_rate]]['num_infected'] for art_rate in sorted(globals()['ba_degree_%s_vs_art' % degree2str[d]].keys())} for d in degrees}
            xvals = []; yvals = []
            for exp_time_art in EXP_TIME_ART:
                xvals += [exp_time_art]*len(num_singletons[exp_degree][exp_time_art][t])
                yvals += [num_singletons[exp_degree][exp_time_art][t][i]/num_infected[exp_degree][exp_time_art][i] for i in range(len(num_singletons[exp_degree][exp_time_art][t]))]
            ax = sns.pointplot(x=xvals, y=yvals, ax=axes[row,col], color=cn_model_pal[m], scale=0.5)
            ax.invert_xaxis()
#plt.ylim(ymin=0,ymax=1.)
#plt.yticks(range(0,100001,int(100000/4)))
for col in range(NUM_EXP_DEGREES):
    axes[0,col].set_title("Expected Degree %d" % degrees[col])
for row in range(NUM_THRESHOLDS):
    axes[row,-1].set_ylabel("Threshold: %s %s" % (int(THRESHOLDS[row]),{True:'year',False:'years'}[THRESHOLDS[row] == 1.]), rotation=0, labelpad=60)
    axes[row,-1].yaxis.set_label_position("right")
plt.suptitle("Proportion of Singletons vs. Expected Time to ART")
fig.text(0.5, 0.03, "Expected Time to ART", ha='center', va='center')
fig.text(0, 0.5, "Proportion of Singletons", ha='center', va='center', rotation='vertical')
legend = plt.legend(handles=cn_model_handles,bbox_to_anchor=(-0.2, -0.6), loc='upper right', borderaxespad=0., frameon=True, ncol=3)
fig.tight_layout(rect=[0, 0.03, 1, 0.95])
plt.show()
fig.savefig('proportion_singletons.pdf', format='pdf', bbox_inches="tight")

### plot number of true singletons and clusters ###
NUM_EXP_DEGREES = len(degrees)
EXP_TIME_ART = [1./art_rate for art_rate in sorted(default_vs_art.keys())]
EXP_TIME_ART_STR = [r'$\frac{1}{%d}$'%int(1./e) if e < 1 else str(int(e))  for e in EXP_TIME_ART]
NUM_EXP_TIME_ART = len(EXP_TIME_ART)
THRESHOLDS = [t for t in sorted({float(k.split('_')[3]) for k in data['HIV_FAVITES_DEFAULT'].keys() if 'LENGTHCLADE' in k and 'NOSINGLETONS' not in k})]
NUM_THRESHOLDS = len(THRESHOLDS)
for s_or_c in ('Singletons','Clusters'):
    fig, axes = plt.subplots(NUM_THRESHOLDS, NUM_EXP_DEGREES, sharex=True, sharey=True, figsize=(14, 16))
    for row in range(NUM_THRESHOLDS):
        t = THRESHOLDS[row]
        for col in range(NUM_EXP_DEGREES):
            exp_degree = degrees[col]
            for m in ('ba','er','ws'):
                NUM_S_OR_C = {d:{1./art_rate:{int(k.split('_')[-1]):v for k,v in data[globals()['%s_degree_%s_vs_art' % (m,degree2str[d])][art_rate]].items() if k.startswith('NUM_%s_LENGTHCLADE_'%s_or_c.upper())} for art_rate in sorted(globals()['ba_degree_%s_vs_art' % degree2str[d]].keys())} for d in degrees}
                xvals = []; yvals = []
                for exp_time_art in EXP_TIME_ART:
                    xvals += [exp_time_art]*len(NUM_S_OR_C[exp_degree][exp_time_art][t])
                    yvals += NUM_S_OR_C[exp_degree][exp_time_art][t]
                ax = sns.pointplot(x=xvals, y=yvals, ax=axes[row,col], color=cn_model_pal[m], scale=0.5)
                ax.invert_xaxis()
    if s_or_c == 'Clusters':
        plt.ylim(ymin=1,ymax=10000)
        plt.yscale("log")
    else:
        plt.ylim(ymin=0,ymax=100000)
        plt.yticks(range(0,100001,int(100000/4)))
    for col in range(NUM_EXP_DEGREES):
        axes[0,col].set_title("Expected Degree %d" % degrees[col])
    for row in range(NUM_THRESHOLDS):
        axes[row,-1].set_ylabel("Threshold: %s %s" % (int(THRESHOLDS[row]),{True:'year',False:'years'}[THRESHOLDS[row] == 1.]), rotation=0, labelpad=60)
        axes[row,-1].yaxis.set_label_position("right")
    plt.suptitle("Number of %s vs. Expected Time to ART"%s_or_c)
    fig.text(0.5, 0.03, "Expected Time to ART", ha='center', va='center')
    fig.text(0, 0.5, "Number of %s"%s_or_c, ha='center', va='center', rotation='vertical')
    legend = plt.legend(handles=cn_model_handles,bbox_to_anchor=(-0.2, -0.6), loc='upper right', borderaxespad=0., frameon=True, ncol=3)
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    plt.show()
    fig.savefig('num_%s.pdf'%s_or_c.lower(), format='pdf', bbox_inches="tight")

