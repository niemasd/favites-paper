# FAVITES Paper

This GitLab repository contains all simulation-related data for the FAVITES paper. The `figures` directory contains all scripts and postprocessed data for figure generation, the `scripts` directory contains scripts used to perform basic parsing tasks, and the `simulations` directory contains all simulation output.